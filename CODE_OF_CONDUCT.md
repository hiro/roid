This project is governed by [Tor Project's Code of Conduct](https://gitweb.torproject.org/community/policies.git/tree/code_of_conduct.txt).

Adheres to the [Tor Project's Social Conduct](https://gitweb.torproject.org/community/policies.git/tree/social_contract.txt).

Subscribes to [Tor Project's Statement of Values](https://gitweb.torproject.org/community/policies.git/tree/statement_of_values.txt).

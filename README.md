# Welcome to Roid

The Roid Project is a collection of resources to maintain and run Onion Services.

- roid is a command line interface to configure, maintain and test onion services

- roid-heroku is a Docker configuration that will help you to install and run an
onion service on Heroku

- roid-terraform contains configuration to deploy a simple onion service via
Terraform and Packer to different infrastructure.

<!---

This file serves as an entry point for GitHub's Contributing
Guidelines [1] only.

GitHub doesn't render rST very well, especially in respect to internal
hyperlink targets and cross-references [2]. People also tend to
confuse rST and Markdown syntax. Therefore, instead of keeping the
contents here (and including from rST documentation under doc/), link
to the Sphinx generated docs is provided below.


[1] https://github.com/blog/1184-contributing-guidelines
[2] https://docutils.sourceforge.io/docs/user/rst/quickref.html#hyperlink-targets

-->

# Roid Contributing Guide

Hi! Welcome to the Roid project. We look forward to collaborating with you.

If you're reporting a bug in Roid, please make sure to include:
 - The version of Droid you're running.
 - The operating system you're running it on.
 - The commands you ran.
 - What you expected to happen, and
 - What actually happened.

If you're a developer, we would be very happy to read your feature idea via a issue
or some code in a Merge Request.

# What is a Roid?

A droid is a fictional robot possessing some degree of artificial intelligence in
the Star Wars science-fiction franchise. The word droid is a trademark which means
that it cannot be used, so we used roid instead.

https://en.wikipedia.org/wiki/Droid_(Star_Wars)

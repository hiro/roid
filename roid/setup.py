import codecs
from distutils.version import LooseVersion
import os
import re
import sys

from setuptools import __version__ as setuptools_version
from setuptools import find_packages
from setuptools import setup

def read_file(filename, encoding='utf8'):
    """Read unicode from given file."""
    with codecs.open(filename, encoding=encoding) as fd:
        return fd.read()


here = os.path.abspath(os.path.dirname(__file__))

# read version number (and other metadata) from package init
init_fn = os.path.join(here, 'roid', '__init__.py')
meta = dict(re.findall(r"""__([a-z]+)__ = '([^']+)""", read_file(init_fn)))

readme = read_file(os.path.join(here, 'README.rst'))
version = meta['version']

install_requires = [
    'configobj',
    'invoke',
    'setuptools',
]

setuptools_known_environment_markers = (LooseVersion(setuptools_version) >= LooseVersion('36.2'))

if setuptools_known_environment_markers:
    install_requires.append('mock ; python_version < "3.3"')
elif 'bdist_wheel' in sys.argv[1:]:
    raise RuntimeError('Error, you are trying to build certbot wheels using an old version '
                       'of setuptools. Version 36.2+ of setuptools is required.')
elif sys.version_info < (3,3):
    install_requires.append('mock')

dev_extras = [
    'coverage',
    'pytest',
    'pytest-cov',
    'pytest-xdist',
    'tox',
    'twine',
    'wheel',
]

dev3_extras = [
    'mypy',
    'pylint',
]

docs_extras = [
    'Sphinx>=1.2',
    'sphinx_rtd_theme',
]


setup(
    name='roid',
    version=version,
    description="roid client for onions",
    long_description=readme,
    url='https://github.torproject.org/tpo/roid',
    author="Hiro",
    author_email='hiro@torproject.org',
    license='Apache License 2.0',
    python_requires='>=2.7, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, !=3.4.*, !=3.5.*',
    classifiers=[
        'Environment :: Console',
        'Environment :: Console :: Curses',
        'Intended Audience :: System Administrators',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Security',
        'Topic :: System :: Installation/Setup',
        'Topic :: System :: Networking',
        'Topic :: System :: Systems Administration',
        'Topic :: Utilities',
    ],

    packages=find_packages(exclude=['docs', 'examples', 'tests', 'venv']),
    include_package_data=True,

    install_requires=install_requires,
    extras_require={
        'dev': dev_extras,
        'dev3': dev3_extras,
        'docs': docs_extras,
    },

    entry_points={
        'console_scripts': [
            'roid = roid.main',
        ],
    },
)

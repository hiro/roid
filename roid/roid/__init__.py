
"""Roid client."""
"""Roid main public entry point."""
from .roid import Roid

# version number like 1.2.3a0, must have at least 2 parts, like 1.2
__version__ = '0.0.1.dev0'


def main():
    """Run roid.
    :param cli_args: command line to Roid, defaults to ``sys.argv[1:]``
    :type cli_args: `list` of `str`
    :returns: value for `sys.exit` about the exit status of Roid
    :rtype: `str` or `int` or `None`
    """
    config = {
        'version': __version__,
    }
    cli = Roid(config)
    cli.program.run()

if __name__ == "__main__":
    main()

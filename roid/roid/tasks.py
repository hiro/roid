import os
import inspect

from invoke import task

def get_resource_path(filename):
    """
    Returns the absolute path of a resource, regardless of whether OnionShare is installed
    systemwide, and whether regardless of platform
    """

    # Look for resources directory relative to python file
    prefix = os.path.join(
        os.path.dirname(
            os.path.dirname(
                os.path.abspath(inspect.getfile(inspect.currentframe()))
            )
        ),
        "share",
    )
    if not os.path.exists(prefix):
        # While running tests during stdeb bdist_deb, look 3 directories up for the share folder
        prefix = os.path.join(
            os.path.dirname(
                os.path.dirname(os.path.dirname(os.path.dirname(prefix)))
            ),
            "share",
        )



    return os.path.join(prefix, filename)

@task(help = {
        'vanguards': 'Install the vanguards package',
        'nginx': 'Install the nginx package'
    })
def install(c, vanguards=False, nginx=False):
    """
       Install required packages
    """

    print("Installing packages")

    c.run("apt-get update")
    c.run("apt-get install -y apt-transport-https lsb-release ca-certificates dirmngr")

    c.run("gpg --import {}".format(get_resource_path("A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc")))
    c.run("gpg --export A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89 | apt-key add -")

    c.run("echo \"deb https://deb.torproject.org/torproject.org buster main\" > /etc/apt/sources.list.d/tor.list")
    c.run("echo \"deb-src https://deb.torproject.org/torproject.org buster main\" >> /etc/apt/sources.list.d/tor.list")
    c.run("echo \"deb https://deb.torproject.org/torproject.org tor-nightly-master-buster main\" >> /etc/apt/sources.list.d/tor.list")
    c.run("echo \"deb-src https://deb.torproject.org/torproject.org tor-nightly-master-buster main\" >> /etc/apt/sources.list.d/tor.list")

    if nginx:
        c.run("echo \"deb https://nginx.org/packages/debian/ buster nginx\" > /etc/apt/sources.list.d/nginx.list")
        c.run("apt-key add {}".format(get_resource_path("nginx_signing.key")))

    c.run("apt-get update")
    c.run("apt-get install -y tor nyx")


    if nginx:
        c.run("apt-get install nginx")

    if vanguards:
        c.run("apt-get install -y vanguards")


@task(help = {
        'port': 'Onion service port',
        'uri': 'Resource identifier on the system, ex: 127.0.0.1:8080 or unix:/var/run/nginx-onion-80.sock',
        'path': 'Onion service path'
    })
def torrc(c, port=False, uri=False, path=False):
    """
       Configure torrc
    """

    print("Configuring torrc")
    print("Stop tor and wait 10 seconds")
    c.run("service tor stop")
    c.run("sleep 10")
    print("Copy torrc")
    c.run("cp {} /etc/tor/torrc".format(get_resource_path("torrc")))
    print("Configure service port torrc")
    c.run("sed -i \"s/TorPort/{}/g\" /etc/tor/torrc".format(port or "80"))
    print("Configure system uri in torrc")
    c.run("sed -i \"s/SystemUri/{}/g\" /etc/tor/torrc".format(uri or "unix:\/var\/run\/nginx-onion-80.sock"))
    c.run("mkdir -p /srv/tor")
    c.run("chown debian-tor:debian-tor /srv/tor")
    c.run("mkdir -p /srv/tor/onion_service")
    c.run("chown debian-tor:debian-tor /srv/tor/onion_service")
    c.run("chmod 700 /srv/tor/onion_service")
    print("Start tor and wait 10 seconds")
    c.run("service tor start")
    c.run("sleep 20")
    c.run("/usr/sbin/update-rc.d -f tor defaults")
    c.run("echo \"This is the onion address: $(cat /srv/tor/onion_service/hostname)\"")

@task
def web_server(c):
    """
       Configure Nginx
    """

    print("Configuring Nginx")

    c.run("cp {} /etc/nginx/nginx.conf".format(get_resource_path("nginx.conf")))
    c.run("sed -i \"s/OnionAddress/$(cat /srv/tor/onion_service/hostname)/g\" /etc/nginx/nginx.conf")
    c.run("nginx -s stop")
    c.run("nginx")
    c.run("/usr/sbin/update-rc.d -f nginx defaults")


@task
def onion_balance(c):
    printf("Configure Onion Balance")
    printf("Coming soon")

@task
def onion_scan(c):
    print("Check with onionscan")
    printf("Coming soon")

@task
def status_check(c):
    print("Install status check")
    printf("Coming soon")

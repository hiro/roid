from __future__ import print_function
from invoke import Collection, Program, Argument
from . import tasks

import logging.handlers
import sys

USER_CANCELLED = ("User chose to cancel the operation and may "
                  "reinvoke the client.")


class Roid(object):

    def __init__(self, config):
        self.logger = logging.getLogger(__name__)
        self.program = Program(namespace=Collection.from_module(tasks), version=config['version'])
